
//!IMPORT
$(document).ready(function () {
	$(".header").height($(window).height());
});

let scaleChoice = true;
let allCols = [];
let sortedByValueArray = [];
let newObj = [];
let uploadedFile = false;
let num = null;
let currentSelection = 0;


function Upload() {
	let fileUpload = document.getElementById("fileUpload");
	let regex = /^([a-zA-Z0-9\s_\\.\-:()])+(.csv|.txt)$/;
	if (regex.test(fileUpload.value.toLowerCase())) {
		if (typeof FileReader != "undefined") {
			let reader = new FileReader();
			reader.onload = function (e) {
				singleData = e.target.result.split("\n");

				// Removes \r and \n from array
				let newstr = "";
				let idValue = 1;
				singleData.forEach((element) => {
					newstr = element.replace(/(\r\n|\n|\r)/gm, "");
					buildingObj = newstr.split(",");
					newObj = {
						id: idValue,
						x: buildingObj[0],
						y: buildingObj[1],
						value: buildingObj[2],
					};
					allCols.push(newObj);
					idValue++;
				});

				uploadedFile = true;
				// Will draw list inside of Display
				displayList();
			};
			reader.readAsText(fileUpload.files[0]);
		} else {
			swal({
				title: "Upload",
				text: `Ihr Browser unterstützt kein HTML5.`,
				buttons: "OK",
			});
		}
	} else {
		swal({
			title: "Upload",
			text: `Bitte wählen Sie eine gültige CSV Datei.`,
			buttons: "OK",
		});
	}
}

function scaleCheckbox() {

	let box1 = document.getElementById("scaleByValue");


	if (box1.checked) {
		scaleChoice = true;
	} else {
		scaleChoice = false;
	}

	this.draw();
}

//!DRAW Canvas
function draw() {
    // Check if file allready is uploaded
    if (uploadedFile) {
        // Create Canvas
        const canvas = document.getElementById("myCanvas");
        const ctx = canvas.getContext("2d");
		ctx.clearRect(0, 0, canvas.width, canvas.height);

		// Sort array to only show points below slidervalue
		let tempArray = [];

		allCols.forEach((element) => {
			if (smallerThen(element.value)) {
				tempArray.push(element);
			}
		})

		// Save tempArray into global sorted Array
		sortedByValueArray = tempArray;


        tempArray.forEach((element) => {
            let x = element.x;
            let y = element.y;
			let value = element.value;

			if (scaleChoice) {
				value = value / 125;
				if (value > 75) {
					value = 75;
				} else if (value < 5) {
					value = 5;
				}
			} else {
				value = 10;
			}

			ctx.fillStyle = this.colorGrading(element.value);
			ctx.beginPath();
			ctx.arc(x, y, value, 0, 2 * Math.PI)
			ctx.closePath();
			ctx.fill();

        });
    } else {
      swal({
        title: "Darstellung",
        text: `Bitte importieren Sie zuerst eine Datei.`,
        buttons: "OK",
    });
    }
}

//!CREATE
function addOnList() {
	if(num == null){
	num = allCols.length;
	}
	num++;
	let xPos = document.getElementById("xPos").value;
	let yPos = document.getElementById("yPos").value;
	let wert = document.getElementById("wert").value;
	if(xPos == "" || yPos == "" || wert == ""){
		swal({
			title: "Erstellen",
			text: `Bitte füllen die Achsen und den Wert aus.`,
			buttons: "OK",
		});
	} else {
	let array = {
		id: num,
		x: xPos,
		y: yPos,
		value: wert,
	};
	allCols.push(array);
	deleteInputs();
	displayList();
}
}

function selectTableRow() {
	let index,trs = document.querySelectorAll("tr");
	for (let i = 0; i < trs.length; i++)
		(function (e) {
			trs[e].addEventListener("click", function () {
				document.getElementById("xPos").value = allCols[e].x;
				document.getElementById("yPos").value = allCols[e].y;
				document.getElementById("wert").value = allCols[e].value;
				document.getElementById("punkt-nr").value = allCols[e].id;
		
		
			// make head unselectable
		if (this.rowIndex !== 0) {
			if (typeof index !== "undefined") {
				trs[index].classList.toggle("selectedElement");
			  }
			  // get selected index
			  index = this.rowIndex;
			  currentSelection = this.rowIndex;
			  
			  // add selected class to index
			  this.classList.toggle("selectedElement");
		}
        

			}, false);
		})(i);
}

//!DISPLAY LIST
function displayList() {
	draw();
	
	if (uploadedFile) {

		let parentElement = document.getElementById("pointList");
		while (parentElement.firstChild) {
			parentElement.removeChild(parentElement.firstChild);
		}

		sortedByValueArray.forEach(element => {
			let number = document.createTextNode(element.id);
			/* let number = document.createTextNode(sortedByValueArray.indexOf(element) + 1); */
			let x = document.createTextNode(element.x);
			let y = document.createTextNode(element.y);
			let v = document.createTextNode(element.value);
			let tableRow = document.createElement("tr");
			let tableData = document.createElement("td");
			let tableData1 = document.createElement("td");
			let tableData2 = document.createElement("td");
			let tableData3 = document.createElement("td");

			document.getElementById("pointList").appendChild(tableRow);
			tableRow.appendChild(tableData);
			tableData.appendChild(number);
			tableRow.appendChild(tableData1);
			tableData1.appendChild(x);
			tableRow.appendChild(tableData2);
			tableData2.appendChild(y);
			tableRow.appendChild(tableData3);
			tableData3.appendChild(v);
		});
		selectTableRow();
	} else {
		swal({
			title: "Aktualisieren",
			text: `Bitte wählen Sie zuerst eine Datei.`,
			buttons: "OK",
	});
}
draw();
}

//!SAVE
function editPoint() {
	if(document.getElementById("punkt-nr").value.length == 0){
		swal({
			title: "Speichern",
			text: `Bitte wählen Sie zuerst einen Punkt aus.`,
			buttons: "OK",
		});
	} else {
	let pointNumber = currentSelection;
	allCols[pointNumber].x = document.getElementById("xPos").value;
	allCols[pointNumber].y = document.getElementById("yPos").value;
	allCols[pointNumber].value = document.getElementById("wert").value;
	deleteInputs();
	displayList();
	}
}

//!DELETE
function deletePoint() {
	if (document.getElementById("punkt-nr").value == 0) {
		swal({
			title: "Löschen",
			text: `Bitte wählen Sie zuerst einen Punkt aus.`,
			buttons: "OK",
		})
	} else {
		let pointNumber = currentSelection;
		allCols.splice(pointNumber, 1);
		deleteInputs();
		displayList();
	}
}

//!STATISTIK
$("#statistic").click(() => {
	if (uploadedFile) {
	  swal({
		title: "Statistik",
		text: `Anzahl Punkte: ${sortedByValueArray.length}\nMinimalwert: ${minimum()}\nMaximalwert: ${maximum()}\nMittelwert: ${middle()}\nMedian: ${median()}`,
		buttons: "OK",
		});
	} else {
	  swal({
    title: "Statistik",
		text: `Sie müssen eine Datei importieren um die Statistik zu laden.`,
    buttons: "OK",
		});
	}
  });

//!DELETE INPUTFIELDS
	function deleteInputs(){
		document.getElementById("xPos").value = "";
		document.getElementById("yPos").value = "";
		document.getElementById("wert").value = "";
		document.getElementById("punkt-nr").value = "";
	}

//!CALCULATIONS START
	function minimum() {
		let minimal = allCols[0].value;
		sortedByValueArray.forEach((element) => {
			if (minimal > element.value) {
				minimal = element.value;
			}
		});
		return minimal;
}

function maximum() {
	let maximal = 0;
	sortedByValueArray.forEach((element) => {
		if (maximal < element.value) {
			maximal = element.value;
		}
	});
	return maximal;
}

function middle() {
    let middlevalue = 0;
    let calculation = 0;
    sortedByValueArray.forEach((element) => {
        calculation += parseInt(element.value);
    });
    middlevalue = calculation / allCols.length;

    if (!middlevalue) {
        return 0;
    }

    return middlevalue;
}

function median() {

	const sortedArray = sortedByValueArray.sort((a, b) => {
		return a.value - b.value;
	});

	const mid = Math.ceil(sortedArray.length / 2);

	let realMedian = sortedArray[mid];
	let belowMedian = sortedArray[mid - 1];
	const median =
		sortedArray.length % 2 == 0 ?
		((parseInt(realMedian.value) + parseInt(belowMedian.value)) / 2) :
		parseInt(sortedArray[mid - 1].value);
	return median;
}
//!CALCULATIONS END

//!EXPORT

//!DOWNLOAD GRAPHIC
function captureGraphic() {
  if (uploadedFile) {
    const download = document.getElementById("downloadGraphic");
    const canvas = document.querySelector("canvas");
  
    let image = canvas.toDataURL("image/png");
  
    download.setAttribute("href", image);
  } else {
    swal({
      title: "Grafik Exportieren",
      text: `Sie müssen eine Datei importieren um die Grafik zu exportieren.`,
      buttons: "OK",
      });
  }
}

//!DOWNLOAD CSV
function captureCSV() {

  if (uploadedFile) {
	// Convert Array to CSV

	let newCols = allCols.map((element) => { return { x: element.x, y: element.y, value: element.value} })

    const csvData = objectToCsv(newCols);

    // Generate URL
    const blob = new Blob([csvData], { type: 'text/csv'})
    const url = window.URL.createObjectURL(blob);
    
    const download = document.getElementById("downloadCSV");
    download.setAttribute("href", url);
    download.setAttribute('download', 'Elektro-Graphic.csv');
  } else {
    swal({
      title: "CSV Exportieren",
      text: `Sie müssen eine Datei importieren um die CSV-Datei zu exportieren.`,
      buttons: "OK",
      });
  }
  }

// Convert an Array into CSV data
function objectToCsv(data) {

	const csvRows = [];

	// Headers
	const headers = Object.keys(data[0]);
	csvRows.push(headers.join(','));

	for (const row of data) {
		const values = headers.map(header => {
			const escaped = ('' + row[header]).replace(/"/g, '\\"');
			return `${escaped}`;

		});
		csvRows.push(values.join(','));
	}

	return csvRows.join('\n');

}

//!WINDOW RESIZER
window.addEventListener("resize", function () {
	if (window.matchMedia("(min-width: 1800px)").matches) {
		canvas.width = 1000;
		canvas.height = 800;
	} else if (window.matchMedia("(min-width: 1200px)").matches) {
		canvas.width = 800;
		canvas.height = 600;
	} else if (window.matchMedia("(min-width: 1000px)").matches) {
		canvas.width = 600;
		canvas.height = 400;
	} else {
		canvas.width = 800;
		canvas.height = 600;
	}
});

//!SLIDER
let slider = document.getElementById("myRange");
let output = document.getElementById("value");
output.innerHTML = slider.value; // Display the default slider value

// Update the current slider value (each time you drag the slider handle)
slider.oninput = function () {
	output.innerHTML = this.value;
	displayList();
};

function smallerThen(importValue) {
	let currentVal = (+slider.value);
	if (importValue <= currentVal) {
		return true;
	} else {
		return false;
	}
}

//!Color-Grading for points
function colorGrading(colorScale) {
	let colorStyle = "";

	if (colorScale < 500) {
		colorStyle = "#a4f6f9";

	} else if (colorScale <= 1000) {
		colorStyle = "#5cf0f5";

	} else if (colorScale <= 1500) {
		colorStyle = "#0fdfe6";

	} else if (colorScale <= 2000) {
		colorStyle = "#0dc3c9";

	} else if (colorScale <= 2500) {
		colorStyle = "#0bacb1";

	} else if (colorScale <= 3000) {
		colorStyle = "#0a9ea3";

	} else if (colorScale <= 3500) {
		colorStyle = "#098286";

	} else if (colorScale <= 4000) {
		colorStyle = "#076b6e";

	} else if (colorScale <= 4500) {
		colorStyle = "#065456";

	} else if (colorScale <= 5000) {
		colorStyle = "#064956";

	} else if (colorScale <= 5500) {
		colorStyle = "#063f5b";

	} else if (colorScale <= 6000) {
		colorStyle = "#06355b";

	} else if (colorScale <= 6500) {
		colorStyle = "#073369";

	} else if (colorScale <= 7000) {
		colorStyle = "#072669";

	} else if (colorScale <= 7500) {
		colorStyle = "#0a0769";

	} else if (colorScale <= 8000) {
		colorStyle = "#190773";

	} else if (colorScale <= 8500) {
		colorStyle = "#290773";

	} else if (colorScale <= 9000) {
		colorStyle = "#3a0773";
	} else if (colorScale <= 9500) {
		colorStyle = "#46088c";

	} else if (colorScale > 9500) {
		colorStyle = "#220443";
	}

	return colorStyle;

}